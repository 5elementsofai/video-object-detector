FROM python:3.7

WORKDIR /app

ENV FLASK_APP app.py
ENV FLASK_RUN_HOST 0.0.0.0

COPY requirements.txt requirements.txt
COPY requirements-gpu.txt requirements-gpu.txt

RUN pip install -r requirements.txt
RUN pip install -r requirements-gpu.txt

COPY . /app

CMD ["flask", "run"]